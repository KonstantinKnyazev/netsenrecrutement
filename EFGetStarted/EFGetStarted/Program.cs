﻿using System;
using System.Linq;

namespace EFGetStarted
{
    class Program
    {
        static void Main()
        {
            //using (var db = new BloggingContext())
            //{
                // Create
                Console.WriteLine("Inserting a new blog");
                //db.Add(new Blog { Url = "sss" });
                //db.SaveChanges();
                ////create
                using (BloggingContext db = new BloggingContext())
                {
                Blog blog1 = new Blog { Url = "url1" };
                Blog blog2 = new Blog { Url = "url2" };

                //create
                //db.Users.Add(users1);
                //db.Users.Add(users2);
                //db.SaveChanges();

                db.Blogs.AddRange(blog1, blog2);

                Author a1 = new Author { Nom = "Tim", Prenom = "Tin" };
                Author a2 = new Author { Nom = "Bob", Prenom = "Marley" };
                Author a3 = new Author { Nom = "Alice", Prenom = "Smith" };
                Author a4 = new Author { Nom = "Jack", Prenom = "Daniels" };


                db.Authors.AddRange(a1, a2, a3, a4);

                db.SaveChanges();

                //}

                // Read
                Console.WriteLine("Querying for a blog");
                //var blog = db.Blogs
                //    .OrderBy(b => b.BlogId)
                //    .First();

                var list = db.Blogs.ToList();
                Console.WriteLine("Data in table");
                foreach (var item in list)
                {
                    Console.WriteLine($"{item.BlogId}.{item.Url}");
                }

                var list1 = db.Authors.ToList();
                Console.WriteLine("Authors");
                foreach (var item in list1)
                {
                    Console.WriteLine($"{item.AuthorId}.{item.Nom}-{item.Prenom}");
                }




                // Update
                //Console.WriteLine("Updating the blog and adding a post");
                //blog1.Url = "mmm";
                //blog1.Posts.Add(
                //    new Post
                //    {
                //        Title = "Hello World",
                //        Content = "I wrote an app using EF Core!"
                //    });
                //db.SaveChanges();

                // Delete
                //Console.WriteLine("Delete the blog");
                //db.Remove(blog1);
                //db.SaveChanges();
            }
        }
    }
}