﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;

namespace EFGetStarted
{
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Commentaire> Commentaires { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite("Data Source=blogging.db");
        
    }

    

    public class Blog
    {
        public int BlogId { get; set; }
        public string Url { get; set; }

        public List<Post> Posts { get; } = new List<Post>();
    }

    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public int BlogId { get; set; }
        public Blog Blog { get; set; }
        public int AuthorId { get; set; }
        public Author Author { get; set; }
        public List<Commentaire> Commentaires { get; } = new List<Commentaire>();


    }

    public class Author
    {
        public int AuthorId { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Photo { get; set; }
        public List<Post> Posts { get; } = new List<Post>();
        public List<Commentaire> Commentaires { get; } = new List<Commentaire>();
    }
    
    public class Commentaire
    {
        public int CommentaireId { get; set; }
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public Author Author { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }

    }
}
