﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Notes
{
    class Test : INotifyPropertyChanged
    {

        private MainPage mainPage;       

        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand SaveCommand { get; set; }
        public ICommand DeleteCommand { get; set; }


        

        public Test(MainPage mainPage)
        {
            this.mainPage = mainPage;

            SaveCommand = new Command(execute: () =>
                {
                    mainPage.save();
                });

            DeleteCommand = new Command(execute: () =>
                {
                    mainPage.deleteAsync();
                });
        }
    }
}
