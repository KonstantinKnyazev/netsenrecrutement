﻿using System;
using System.IO;
using Xamarin.Forms;

namespace Notes
{
    public partial class MainPage : ContentPage
    {
        string _fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "notes.txt");
        private Test test { get; set; }

        public MainPage()
        {
            InitializeComponent();

            if (File.Exists(_fileName))
            {
                editor.Text = File.ReadAllText(_fileName);
            }

            test = new Test(this);
            BindingContext = test;
        }

        //void OnSaveButtonClicked(object sender, EventArgs e)
        //{
        //    File.WriteAllText(_fileName, editor.Text);
        //}

        //void OnDeleteButtonClicked(object sender, EventArgs e)
        //{
        //    if (File.Exists(_fileName))
        //    {
        //        File.Delete(_fileName);
        //    }
        //    editor.Text = string.Empty;
        //}

        public void save()
        {
            DisplayAlert("Warning", "You will save " +editor.Text, "OK");
            File.WriteAllText(_fileName, editor.Text);
        }

        internal async System.Threading.Tasks.Task deleteAsync()
        {
            bool result = await DisplayAlert("Warning", "Confirm deleting", "Yes", "No");
            if (result)
            {
                if (File.Exists(_fileName))
                {
                    File.Delete(_fileName);
                }
                editor.Text = string.Empty;
            }

        }
    }
}